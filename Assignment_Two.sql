 -- 01_dbCreate.sql
use master
go
drop database SUPERHEROESDB
go

IF (NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'SUPERHEROESDB'))
	BEGIN
		CREATE DATABASE SUPERHEROESDB
    END
	go

-- 02_tableCreate.sql

use SUPERHEROESDB
go

IF (NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUPERHERO'))
	BEGIN
		create table SUPERHERO(
		ID int identity(1,1) not null primary key,
		NAME nvarchar(50) not null,
		ALIAS nvarchar(50) not null,
		ORIGIN nvarchar(50) not null
		)
    END
go

IF (NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ASSISTANT'))
	BEGIN
		create table ASSISTANT(
		ID int not null identity(1,1) primary key,
		NAME nvarchar(50) not null unique --Unique constraint to enforce one to many relationship
		)
	end
go

IF (NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'POWER'))
	BEGIN
		create table POWER(
		ID int not null identity(1,1) primary key,
		NAME nvarchar(50) not null,
		DESCRIPTION nvarchar(255) not null
		)
	end
go

--03_relationshipSuperheroAssistant.sql
if(not exists (select * from INFORMATION_SCHEMA.TABLE_CONSTRAINTS
			   where TABLE_NAME = 'ASSISTANT' 
			   and CONSTRAINT_TYPE = 'FOREIGN KEY' 
			   and CONSTRAINT_NAME = 'FK_ASSISTANT_SUPERHERO')
	and exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUPERHERO')) 
BEGIN
	alter table ASSISTANT
	add SUPERHERO_ID int not null;
	alter table ASSISTANT
	add constraint FK_ASSISTANT_SUPERHERO foreign key(SUPERHERO_ID) references SUPERHERO(ID)
	on delete no action
	on update cascade;
END
go

--04_relationshipSuperheroPower.sql
IF (NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUPERHERO_POWER')
	AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUPERHERO')
	AND EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'POWER'))
BEGIN
	create table SUPERHERO_POWER(
		SUPERHERO_ID int not null
		constraint FK_SUPERHERO_SUPERHEROPOWER
		references SUPERHERO(ID)
		on delete no action
		on update cascade,
		POWER_ID int not null
		constraint FK_POWER_SUPERHEROPOWER
		references POWER(ID)
		on delete no action
		on update cascade,
		primary key(SUPERHERO_ID, POWER_ID)
	)
end
go

--05_insertSuperheroes.sql
if(exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SUPERHERO'))
begin 
	insert into SUPERHERO(NAME, ALIAS, ORIGIN)
	values 
		('Jens', 'Politikeren', 'Oslo'),
		('Petter', 'Fiskemannen', 'Bergen'),
		('Karl','Vepsemannen','Australia')

end 
go

--06_insertAssistants.sql
if(exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SUPERHERO')
	and exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ASSISTANT'))
begin 
	insert into ASSISTANT(NAME, SUPERHERO_ID)
	values 
		('Arne', 1),
		('Birger', 2),
		('Ketil', 3)
end
go

--07_powers.sql
if(exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SUPERHERO')
	and exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'POWER'))
begin 
	insert into POWER(NAME, DESCRIPTION)
	values 
		('Flight', 'Enables flight.'),
		('Super Speed', 'Enables running at incredible speeds.'),
		('Super Strength', 'Lift mountains!'),
		('Sting', 'Grow a stinger.'),
		('Law','Can pass any laws.'),
		('Swim', 'Can swim.');
	insert into SUPERHERO_POWER(SUPERHERO_ID, POWER_ID) 
	values
		(1,5),
		(1,1),
		(2,6),
		(3,5),
		(3,2),
		(1,2);
end
go

--08_updateSuperhero.sql
if (exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SUPERHERO'))
begin
	update SUPERHERO
	set NAME = 'Trond'
	where NAME = 'Jens';
end
go

--09_deleteAssistant.sql
if (exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ASSISTANT'))
begin
	delete ASSISTANT
	where NAME = 'Arne';
end
go