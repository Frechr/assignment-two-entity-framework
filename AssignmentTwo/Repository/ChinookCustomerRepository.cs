﻿using assignment_two.Data_Access;
using AssignmentTwo.Context;
using AssignmentTwo.Model.Generated;
using AssignmentTwo.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customer = AssignmentTwo.Model.Generated.Customer;

namespace AssignmentTwo.Repository
{
	/// <inheritdoc/>
	public class ChinookCustomerRepository : ICustomerRepository
	{
		private ChinookContext dbContext = new();


		public IQueryable<Customer> GetAll()
		{
			try
			{
				return dbContext.Customers;
			}
			catch ( Exception e )
			{
				Console.Error.WriteLine( "Something went wrong when retrieving all customers." );
				Console.Error.WriteLine( e.Message );
				Console.Error.WriteLine( e.StackTrace );
				return new List<Customer>().AsQueryable();
			}

		}

		public Customer Get( int ID )
		{

			try
			{
				return dbContext.Customers.Find( ID );
			}
			catch ( Exception e )
			{
				Console.Error.WriteLine( "Something went wrong when retrieving customer." );
				Console.Error.WriteLine( e.Message );
				Console.Error.WriteLine( e.StackTrace );
				return new Customer() { };
			}

		}

		public Customer GetByFirstName( string search )
		{
			try
			{
				return ( from c in dbContext.Customers
						 where c.FirstName.Contains( search ) == true
						 select c ).First();
			}
			catch ( Exception e )
			{
				Console.Error.WriteLine( "Something went wrong when retrieving customer by name." );
				Console.Error.WriteLine( e.Message );
				Console.Error.WriteLine( e.StackTrace );
				return new Customer() { };
			}
		}


		public bool Add( Customer customer )
		{

			try
			{
				dbContext.Customers.Add( customer );
				dbContext.SaveChanges();
			}
			catch ( Exception e )
			{
				Console.Error.WriteLine( "Something went wrong when adding a customer." );
				Console.Error.WriteLine( e.Message );
				Console.Error.WriteLine( e.StackTrace );
				return false;
			}
			return true;

		}

		public bool Delete( int ID )
		{

			try
			{
				Customer customerToDelete = dbContext.Customers.Find( ID );
				dbContext.Customers.Remove( customerToDelete );
				dbContext.SaveChanges();
			}
			catch ( Exception e )
			{
				Console.Error.WriteLine( "Something went wrong when deleting customer." );
				Console.Error.WriteLine( e.Message );
				Console.Error.WriteLine( e.StackTrace );
				return false;
			}
			return true;

		}


		public bool Update( Customer customer )
		{

			try
			{
				Customer customerToUpdate = dbContext.Customers.Find( customer.CustomerId );
				customerToUpdate = customer;
				dbContext.SaveChanges();
			}
			catch ( Exception e )
			{
				Console.Error.WriteLine( "Something went wrong when updating customer." );
				Console.Error.WriteLine( e.Message );
				Console.Error.WriteLine( e.StackTrace );
				return false;
			}
			return true;

		}

		public IQueryable<CustomerSpender> GetHighestSpenders()
		{

			IQueryable<Customer> highestSpendingCustomers =
				dbContext.Customers.OrderByDescending( c => c.Invoices.Sum( invoice => invoice.Total ) );

			return highestSpendingCustomers.Select( ( c ) => new CustomerSpender( c.AsViewModel(), c.Invoices.Sum( i => i.Total ) ) );

		}


		public CustomerGenre GetCustomerGenre( int ID )
		{

			List<Genre> favoriteGenres = new List<Genre>();

			var customer = dbContext.Customers.First( customer => customer.CustomerId == ID );
			var invoices = customer.Invoices.ToList();

			IEnumerable<Track> tracks = new List<Track>();
			List<string> genreApperances = new List<string>();
			invoices.ForEach( invoice => invoice.InvoiceLines.ToList().ForEach( line => genreApperances.Add( line.Track.Genre.Name ) ) );

			favoriteGenres = favoriteGenres.GroupBy( genre => genre ).OrderByDescending( grp => grp.Count() ).Select( grp => grp.Key ).ToList();

			return new CustomerGenre( customer, favoriteGenres );

		}


		public IQueryable<CustomerCountry> GetCustomerCountries()
		{

			return from customer in dbContext.Customers
				   group customer by customer.Country into customerCountries
				   select new CustomerCountry( customerCountries.Key, customerCountries.ToList() );
		}


		public IQueryable<Customer> GetCustomerPage( int offset, int limit )
		{
			return dbContext.Customers.Take( new Range( offset, offset + limit ) );
		}
	}
}
