﻿using assignment_two.Data_Access;
using AssignmentTwo.Context;
using AssignmentTwo.Model.Generated;
using AssignmentTwo.Repository;
using AssignmentTwo.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Identity.Client.Extensions.Msal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customer = AssignmentTwo.Model.Generated.Customer;

namespace AssignmentTwo.Repository
{
	/// <summary>
	/// Interface for customer-specific repository commands.
	/// </summary> 
	public interface ICustomerRepository : IRepository<Customer>
	{
		/// <summary>
		/// Gets the highest spenders.
		/// </summary> 
		/// <returns>
		///	<c>  	
		///	<see cref="Queryable"/>&lt;<see cref="CustomerGenre"/>&gt;
		///	</c> 
		///	</returns>
		IQueryable<CustomerSpender> GetHighestSpenders();

		/// <summary>
		/// Gets customers by country.
		/// </summary> 
		/// <returns>
		///	<c>  	
		///	<see cref="Queryable"/>&lt;<see cref="CustomerGenre"/>&gt;
		///	</c> 
		///	</returns>
		IQueryable<CustomerCountry> GetCustomerCountries();

		/// <summary>
		/// Gets a customer by name.
		/// </summary> 
		/// <returns>
		///	<see cref="Customer"/> 
		///	</returns>
		Customer GetByFirstName( string name );

		/// <summary>
		/// Gets customers favorite genre(s).
		/// </summary>
		/// <param name="ID">Customer identificator.</param>
		/// <returns>
		/// <code>
		/// <see cref="CustomerGenre"/> 
		/// </code>
		/// </returns>
		CustomerGenre GetCustomerGenre( int ID );

		/// <summary>
		/// Gets customers specified by a offset and limit.
		/// </summary>
		/// <param name="offset">The offset.</param>
		/// <param name="limit">The limit.</param>
		/// <returns>
		///	<see cref="Queryable"/>&lt;<see cref="CustomerGenre"/>&gt;
		/// </returns>
		IQueryable<Customer> GetCustomerPage( int offset, int limit );
	}

}
