﻿using AssignmentTwo.Context;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_two.Data_Access
{
    /// <summary>
    /// Interface for a generic repository.
    /// </summary>
    /// <typeparam name="Repository">The storage type which supports CRUD operations.</typeparam>
    /// <typeparam name="Record">The type og objects which storage's objects contains.</typeparam>
    public interface IRepository<TRecord> where TRecord : class
    {
        /// <summary>
        /// Gets all records from repository.
        /// </summary>
        /// <returns>A generic collection of records.</returns>
        IQueryable<TRecord> GetAll();

        /// <summary>
        /// Adds record to repository.
        /// </summary>
        /// <param name="record">The record to be added.</param>
        /// <returns>A boolean value indicating success.</returns>
        bool Add( TRecord record );

        /// <summary>
        /// Gets single record by ID.
        /// </summary>
        /// <param name="ID">The ID of the retrieving record.</param>
        /// <returns>A single record.</returns>
        TRecord Get( int ID );

        /// <summary>
        /// Updates a record by looking at the records ID.
        /// </summary>
        /// <param name="record">The updating record.</param>
        /// <returns>A boolean value indicating success.</returns>
        bool Update( TRecord record );

        /// <summary>
        /// Deletes a record by ID.
        /// </summary>
        /// <param name="ID">The ID of the deleting record.</param>
        /// <returns>A boolean value indicating success.</returns>
        bool Delete( int ID );

    }
}
