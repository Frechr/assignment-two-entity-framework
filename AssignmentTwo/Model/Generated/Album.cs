﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Album model
/// </summary>
public partial class Album
{
	/// <summary>
	/// Gets or sets the album identifier.
	/// </summary>
	/// <value>
	/// The album identifier.
	/// </value>
	public int AlbumId
	{
		get; set;
	}
	/// <summary>
	/// Gets or sets the title.
	/// </summary>
	/// <value>
	/// The title.
	/// </value>
	public string Title { get; set; } = null!;
	/// <summary>
	/// Gets or sets the artist identifier.
	/// </summary>
	/// <value>
	/// The artist identifier.
	/// </value>
	public int ArtistId
	{
		get; set;
	}
	/// <summary>
	/// Gets or sets the artist.
	/// </summary>
	/// <value>
	/// The artist.
	/// </value>
	public virtual Artist Artist { get; set; } = null!;
	/// <summary>
	/// Gets the tracks.
	/// </summary>
	/// <value>
	/// The tracks.
	/// </value>
	public virtual ICollection<Track> Tracks { get; } = new List<Track>();
}
