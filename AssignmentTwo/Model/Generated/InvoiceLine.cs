﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Invoice line
/// </summary>
public partial class InvoiceLine
{
	/// <summary>
	/// Gets or sets the invoice line identifier.
	/// </summary>
	/// <value>
	/// The invoice line identifier.
	/// </value>
	public int InvoiceLineId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the invoice identifier.
	/// </summary>
	/// <value>
	/// The invoice identifier.
	/// </value>
	public int InvoiceId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the track identifier.
	/// </summary>
	/// <value>
	/// The track identifier.
	/// </value>
	public int TrackId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the unit price.
	/// </summary>
	/// <value>
	/// The unit price.
	/// </value>
	public decimal UnitPrice
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the quantity.
	/// </summary>
	/// <value>
	/// The quantity.
	/// </value>
	public int Quantity
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the invoice.
	/// </summary>
	/// <value>
	/// The invoice.
	/// </value>
	public virtual Invoice Invoice { get; set; } = null!;

	/// <summary>
	/// Gets or sets the track.
	/// </summary>
	/// <value>
	/// The track.
	/// </value>
	public virtual Track Track { get; set; } = null!;
}
