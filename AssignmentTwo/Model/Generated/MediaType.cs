﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// MEdiaType model
/// </summary>
public partial class MediaType
{
	/// <summary>
	/// Gets or sets the media type identifier.
	/// </summary>
	/// <value>
	/// The media type identifier.
	/// </value>
	public int MediaTypeId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>
	/// The name.
	/// </value>
	public string? Name
	{
		get; set;
	}

	/// <summary>
	/// Gets the tracks.
	/// </summary>
	/// <value>
	/// The tracks.
	/// </value>
	public virtual ICollection<Track> Tracks { get; } = new List<Track>();
}
