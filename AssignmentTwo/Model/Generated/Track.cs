﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Track model
/// </summary>
public partial class Track
{
	/// <summary>
	/// Gets or sets the track identifier.
	/// </summary>
	/// <value>
	/// The track identifier.
	/// </value>
	public int TrackId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>
	/// The name.
	/// </value>
	public string Name { get; set; } = null!;

	/// <summary>
	/// Gets or sets the album identifier.
	/// </summary>
	/// <value>
	/// The album identifier.
	/// </value>
	public int? AlbumId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the media type identifier.
	/// </summary>
	/// <value>
	/// The media type identifier.
	/// </value>
	public int MediaTypeId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the genre identifier.
	/// </summary>
	/// <value>
	/// The genre identifier.
	/// </value>
	public int? GenreId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the composer.
	/// </summary>
	/// <value>
	/// The composer.
	/// </value>
	public string? Composer
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the milliseconds.
	/// </summary>
	/// <value>
	/// The milliseconds.
	/// </value>
	public int Milliseconds
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the bytes.
	/// </summary>
	/// <value>
	/// The bytes.
	/// </value>
	public int? Bytes
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the unit price.
	/// </summary>
	/// <value>
	/// The unit price.
	/// </value>
	public decimal UnitPrice
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the album.
	/// </summary>
	/// <value>
	/// The album.
	/// </value>
	public virtual Album? Album
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the genre.
	/// </summary>
	/// <value>
	/// The genre.
	/// </value>
	public virtual Genre? Genre
	{
		get; set;
	}

	/// <summary>
	/// Gets the invoice lines.
	/// </summary>
	/// <value>
	/// The invoice lines.
	/// </value>
	public virtual ICollection<InvoiceLine> InvoiceLines { get; } = new List<InvoiceLine>();

	/// <summary>
	/// Gets or sets the type of the media.
	/// </summary>
	/// <value>
	/// The type of the media.
	/// </value>
	public virtual MediaType MediaType { get; set; } = null!;

	/// <summary>
	/// Gets the playlists.
	/// </summary>
	/// <value>
	/// The playlists.
	/// </value>
	public virtual ICollection<Playlist> Playlists { get; } = new List<Playlist>();
}
