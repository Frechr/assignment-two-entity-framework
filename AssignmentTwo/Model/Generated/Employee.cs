﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Employee model
/// </summary>
public partial class Employee
{
	/// <summary>
	/// Gets or sets the employee identifier.
	/// </summary>
	/// <value>
	/// The employee identifier.
	/// </value>
	public int EmployeeId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the last name.
	/// </summary>
	/// <value>
	/// The last name.
	/// </value>
	public string LastName { get; set; } = null!;

	/// <summary>
	/// Gets or sets the first name.
	/// </summary>
	/// <value>
	/// The first name.
	/// </value>
	public string FirstName { get; set; } = null!;

	/// <summary>
	/// Gets or sets the title.
	/// </summary>
	/// <value>
	/// The title.
	/// </value>
	public string? Title
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the reports to.
	/// </summary>
	/// <value>
	/// The reports to.
	/// </value>
	public int? ReportsTo
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the birth date.
	/// </summary>
	/// <value>
	/// The birth date.
	/// </value>
	public DateTime? BirthDate
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the hire date.
	/// </summary>
	/// <value>
	/// The hire date.
	/// </value>
	public DateTime? HireDate
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the address.
	/// </summary>
	/// <value>
	/// The address.
	/// </value>
	public string? Address
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the city.
	/// </summary>
	/// <value>
	/// The city.
	/// </value>
	public string? City
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the state.
	/// </summary>
	/// <value>
	/// The state.
	/// </value>
	public string? State
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the country.
	/// </summary>
	/// <value>
	/// The country.
	/// </value>
	public string? Country
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the postal code.
	/// </summary>
	/// <value>
	/// The postal code.
	/// </value>
	public string? PostalCode
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the phone.
	/// </summary>
	/// <value>
	/// The phone.
	/// </value>
	public string? Phone
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the fax.
	/// </summary>
	/// <value>
	/// The fax.
	/// </value>
	public string? Fax
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the email.
	/// </summary>
	/// <value>
	/// The email.
	/// </value>
	public string? Email
	{
		get; set;
	}

	/// <summary>
	/// Gets the customers.
	/// </summary>
	/// <value>
	/// The customers.
	/// </value>
	public virtual ICollection<Customer> Customers { get; } = new List<Customer>();

	/// <summary>
	/// Gets the inverse reports to navigation.
	/// </summary>
	/// <value>
	/// The inverse reports to navigation.
	/// </value>
	public virtual ICollection<Employee> InverseReportsToNavigation { get; } = new List<Employee>();

	/// <summary>
	/// Gets or sets the reports to navigation.
	/// </summary>
	/// <value>
	/// The reports to navigation.
	/// </value>
	public virtual Employee? ReportsToNavigation
	{
		get; set;
	}
}
