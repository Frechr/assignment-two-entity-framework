﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Artist model
/// </summary>
public partial class Artist
{
	/// <summary>
	/// Gets or sets the artist identifier.
	/// </summary>
	/// <value>
	/// The artist identifier.
	/// </value>
	public int ArtistId
	{
		get; set;
	}
	/// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>
	/// The name.
	/// </value>
	public string? Name
	{
		get; set;
	}
	/// <summary>
	/// Gets the albums.
	/// </summary>
	/// <value>
	/// The albums.
	/// </value>
	public virtual ICollection<Album> Albums { get; } = new List<Album>();
}
