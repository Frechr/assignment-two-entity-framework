﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Invoice model
/// </summary>
public partial class Invoice
{
	/// <summary>
	/// Gets or sets the invoice identifier.
	/// </summary>
	/// <value>
	/// The invoice identifier.
	/// </value>
	public int InvoiceId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the customer identifier.
	/// </summary>
	/// <value>
	/// The customer identifier.
	/// </value>
	public int CustomerId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the invoice date.
	/// </summary>
	/// <value>
	/// The invoice date.
	/// </value>
	public DateTime InvoiceDate
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the billing address.
	/// </summary>
	/// <value>
	/// The billing address.
	/// </value>
	public string? BillingAddress
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the billing city.
	/// </summary>
	/// <value>
	/// The billing city.
	/// </value>
	public string? BillingCity
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the state of the billing.
	/// </summary>
	/// <value>
	/// The state of the billing.
	/// </value>
	public string? BillingState
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the billing country.
	/// </summary>
	/// <value>
	/// The billing country.
	/// </value>
	public string? BillingCountry
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the billing postal code.
	/// </summary>
	/// <value>
	/// The billing postal code.
	/// </value>
	public string? BillingPostalCode
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the total.
	/// </summary>
	/// <value>
	/// The total.
	/// </value>
	public decimal Total
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the customer.
	/// </summary>
	/// <value>
	/// The customer.
	/// </value>
	public virtual Customer Customer { get; set; } = null!;

	/// <summary>
	/// Gets the invoice lines.
	/// </summary>
	/// <value>
	/// The invoice lines.
	/// </value>
	public virtual ICollection<InvoiceLine> InvoiceLines { get; } = new List<InvoiceLine>();
}
