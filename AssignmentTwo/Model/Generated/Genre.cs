﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Genre model
/// </summary>
public partial class Genre
{
	/// <summary>
	/// Gets or sets the genre identifier.
	/// </summary>
	/// <value>
	/// The genre identifier.
	/// </value>
	public int GenreId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>
	/// The name.
	/// </value>
	public string? Name
	{
		get; set;
	}

	/// <summary>
	/// Gets the tracks.
	/// </summary>
	/// <value>
	/// The tracks.
	/// </value>
	public virtual ICollection<Track> Tracks { get; } = new List<Track>();
}
