﻿using System;
using System.Collections.Generic;

namespace AssignmentTwo.Model.Generated;

/// <summary>
/// Playlist model
/// </summary>
public partial class Playlist
{
	/// <summary>
	/// Gets or sets the playlist identifier.
	/// </summary>
	/// <value>
	/// The playlist identifier.
	/// </value>
	public int PlaylistId
	{
		get; set;
	}

	/// <summary>
	/// Gets or sets the name.
	/// </summary>
	/// <value>
	/// The name.
	/// </value>
	public string? Name
	{
		get; set;
	}

	/// <summary>
	/// Gets the tracks.
	/// </summary>
	/// <value>
	/// The tracks.
	/// </value>
	public virtual ICollection<Track> Tracks { get; } = new List<Track>();
}
