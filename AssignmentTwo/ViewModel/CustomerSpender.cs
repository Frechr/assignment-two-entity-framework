﻿using AssignmentTwo.Context;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CustomerViewModel = AssignmentTwo.ViewModel.Customer;


namespace AssignmentTwo.ViewModel
{
    /// <summary>
    /// A <see cref="CustomerSpender" /> is a composition of the <see cref="CustomerViewModel" /> class and a single property storing the <see cref="Total" /> amount of money spent.
    /// </summary>
    /// <seealso cref="AssignmentTwo.ViewModel.Customer" />
    public class CustomerSpender : CustomerViewModel
    {
        /// <summary>
        /// Simple constructor for creating the view.
        /// </summary>
        /// <param name="customer">The big spender.</param>
        /// <param name="total">Their money.</param>
        /// <see cref=""></see>
        public CustomerSpender( CustomerViewModel customer, decimal total ) : base( customer )
        {
            Total = total;
        }
        /// <summary>
        /// Gets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
        public decimal Total
        {
            get;
        }
        /// <summary>
        /// Converts to a human readable string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return JsonSerializer.Serialize( this, new JsonSerializerOptions() { WriteIndented = true } );
        }
    }
}
