﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CustomerModel = AssignmentTwo.Model.Generated.Customer;
namespace AssignmentTwo.ViewModel
{
	/// <summary>
	/// Composition of a country and its customers.
	/// </summary>
	public class CustomerCountry
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CustomerCountry"/> class.
		/// </summary>
		/// <param name="customers">The customers.</param>
		/// <param name="country">The country.</param>
		public CustomerCountry( string? country, IEnumerable<CustomerModel> customers )
		{
			Customers = customers;
			Country = country;
		}
		/// <summary>
		/// Gets or sets the customers.
		/// </summary>
		/// <value>
		/// The customers.
		/// </value>
		public IEnumerable<CustomerModel> Customers
		{
			get; set;
		}
		/// <summary>
		/// Gets or sets the country.
		/// </summary>
		/// <value>
		/// The country.
		/// </value>
		public string? Country
		{
			get; set;
		}
		/// <summary>
		/// Converts to a human readable string.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return JsonSerializer.Serialize( this, new JsonSerializerOptions() { WriteIndented = true } );
		}
	}
}
