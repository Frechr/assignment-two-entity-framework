﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using GeneratedCustomer = AssignmentTwo.Model.Generated.Customer;

namespace AssignmentTwo.ViewModel
{
    /// <summary>
    /// A subset of the Model.Customer class.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Gets or sets the customer identifier.
        /// </summary>
        /// <value>
        /// The customer identifier.
        /// </value>
        public int CustomerId
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public string Phone
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email
        {
            get; set;
        }

        /// <summary>
        /// Explicit conversion from CustomerViewModel into a Generated Customer Model: (CustomerModel)CustomerView;
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static explicit operator GeneratedCustomer( Customer customerViewModel ) =>
            new GeneratedCustomer()
            {
                CustomerId = customerViewModel.CustomerId,
                FirstName = customerViewModel.FirstName,
                LastName = customerViewModel.LastName,
                Country = customerViewModel.Country,
                Phone = customerViewModel.Phone,
                PostalCode = customerViewModel.PostalCode,
                Email = customerViewModel.Email
            };

        /// <summary>
        /// Creates a new cloned Customer.
        /// </summary>
        /// <param name="c">The c.</param>
        public Customer( Customer c ) : this( c.CustomerId, c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email )
        {
        }
        /// <summary>
        /// Creates a new Customer from a generated customer.
        /// </summary>
        /// <param name="c">The c.</param>
        public Customer( GeneratedCustomer c ) : this( c.CustomerId, c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="country">The country.</param>
        /// <param name="postalCode">The postal code.</param>
        /// <param name="phone">The phone.</param>
        /// <param name="email">The email.</param>
        /// <exception cref="System.ArgumentNullException">
        /// firstName
        /// or
        /// lastName
        /// or
        /// email
        /// </exception>
        public Customer( int customerId, string firstName, string lastName, string? country, string? postalCode, string? phone, string email )
        {
            CustomerId = customerId;
            FirstName = firstName ?? throw new ArgumentNullException( nameof( firstName ) );
            LastName = lastName ?? throw new ArgumentNullException( nameof( lastName ) );
            Country = country;
            PostalCode = postalCode;
            Phone = phone;
            Email = email ?? throw new ArgumentNullException( nameof( email ) );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        public Customer()
        {
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return JsonSerializer.Serialize( this, new JsonSerializerOptions() { WriteIndented = true } );
        }
    }
}
