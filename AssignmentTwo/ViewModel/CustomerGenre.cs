﻿using AssignmentTwo.Model.Generated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CustomerModel = AssignmentTwo.Model.Generated.Customer;
using CustomerViewModel = AssignmentTwo.ViewModel.Customer;

namespace AssignmentTwo.ViewModel
{
	/// <summary>
	/// Composition of customer and genre.
	/// </summary>
	/// <seealso cref="AssignmentTwo.ViewModel.Customer" />
	public class CustomerGenre : CustomerViewModel
	{
		/// <summary>
		/// Gets the favorite genres.
		/// </summary>
		/// <value>
		/// The favorite genres.
		/// </value>
		public List<Genre> FavoriteGenres
		{
			get;
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="CustomerGenre"/> class.
		/// </summary>
		/// <param name="customer">The customer.</param>
		/// <param name="favoriteGenres">The favorite genres.</param>
		public CustomerGenre( CustomerModel customer, List<Genre> favoriteGenres ) : base( customer )
		{
			FavoriteGenres = favoriteGenres;
		}

		/// <summary>
		/// Converts to a human readable string.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return JsonSerializer.Serialize( this, new JsonSerializerOptions() { WriteIndented = true } );
		}
	}
}
