﻿using AssignmentTwo.Model;

using CustomerModel = AssignmentTwo.Model.Generated.Customer;
using CustomerViewModel = AssignmentTwo.ViewModel.Customer;

namespace AssignmentTwo.ViewModel
{
	/// <summary>
	/// Extensions for converting between the view and model.
	/// </summary>
	public static class CustomerExtensions
	{
		/// <summary>
		/// A partial copy of Model.Customer into a full ViewModel.Customer.
		/// </summary>
		/// <param name="customer">The customer.</param>
		/// <returns></returns>
		public static CustomerViewModel AsViewModel( this CustomerModel customer )
		{
			return new CustomerViewModel( customer );
		}

		/// <summary>
		/// A copy of ViewModel.Customer into a Generated.Customer.
		/// </summary>
		/// <param name="vCustomer">The v customer.</param>
		/// <returns></returns>
		public static CustomerModel AsModel( this CustomerViewModel vCustomer )
		{
			return new CustomerModel()
			{
				CustomerId = vCustomer.CustomerId,
				FirstName = vCustomer.FirstName,
				LastName = vCustomer.LastName,
				Country = vCustomer.Country,
				PostalCode = vCustomer.PostalCode,
				Phone = vCustomer.Phone,
				Email = vCustomer.Email
			};
		}
	}
}
