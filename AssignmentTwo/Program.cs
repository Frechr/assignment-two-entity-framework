﻿#define DEBUG 
using assignment_two.Data_Access;
using AssignmentTwo.Context;
using AssignmentTwo.Model.Generated;
using AssignmentTwo.Repository;
using AssignmentTwo.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Runtime.CompilerServices;
using System.Text.Json;
using Customer = AssignmentTwo.ViewModel.Customer;
using CustomerModel = AssignmentTwo.Model.Generated.Customer;

namespace AssignmentTwo
{
    internal class Program
    {
        static void Main( string[] args )
        {
            var repo = new ChinookCustomerRepository();

            var customers = repo.GetAll().ToList();
            customers.ForEach( c => Console.WriteLine( c.AsViewModel() ) );

            Customer customer = new Customer { FirstName = "albert", LastName = "åberg", Email = "not" };
            Console.WriteLine( repo.Add( customer.AsModel() ) ? "Added customer successfully: " : "Failed to add customer." );

            customer.LastName = "Strand";
            Console.WriteLine( repo.Update( customer.AsModel() ) ? "Updated customer successfully." : "Failed to update customer." );

            var getCustomer = repo.Get( 1 );
            Console.WriteLine( getCustomer );

            getCustomer = repo.GetByFirstName( "albert" );
            Console.WriteLine( getCustomer );

            var lastCustomer = customers.Last();
            Console.WriteLine( repo.Delete( lastCustomer.CustomerId ) ? "Deleted customer successfully." : "Failed to delete customer." );

            var c = repo.GetCustomerGenre( 1 );
            Console.WriteLine( c );

            var c2 = repo.GetCustomerCountries();
            c2.ToList().ForEach( Console.WriteLine );

            var c3 = repo.GetHighestSpenders();
            c3.ToList().ForEach( Console.WriteLine );
        }
    }
}